package fibonacci;

import java.util.Scanner;

//creating main class Fibonnaci
public class Fibonacci{
    //create two static variables that will show the start and end of the Fibonacci numbers
    static int endNumber;
    static int startNumber;


    public static void main(String[] args) {
//creating 3 variables to build Fibonacci numbers


        System.out.println("Введите начальное значение последовательности Фибоначчи");
        Scanner scanner1 = new Scanner(System.in);
        startNumber = scanner1.nextInt();
        System.out.println("Введите конечное значение последовательности Фибоначчи");
        Scanner scanner2 = new Scanner(System.in);
        endNumber = scanner2.nextInt();
        int var1;
        int var2 = 1;
        int var3 = 1;

        try {
            while (var3 < endNumber) {
                int i = 0;
                var1 = var2 + var3 / i;
                System.out.print(var1);
                var2 = var3;
                var3 = var1;
                System.out.println();
            }
        } catch (ArithmeticException catchmistake) {
            System.out.println("Exception in thread \"main\" java.lang.ArithmeticException: / by zero\n" +
                    "\tat fibonacci.Fibonacci.main(Fibonacci.java:30)");
            while (var3 < endNumber) {
                int i = 0;
                var1 = var2 + var3 / (i + 1);
                System.out.print(var1);
                var2 = var3;
                var3 = var1;
                System.out.println();
            }
        }


        final int oddSum = showandsumOdd(startNumber, endNumber);
        final int evenSum = showandsumEven(startNumber, endNumber);

        persentagingOddsum(oddSum, evenSum);
        persentagingEvensum(oddSum, evenSum);
    }

    //creating method that will show and sum Odd numbers
    public static int showandsumOdd(int startNumber, int endNumber) {
        int oddSum = 0;
        System.out.println("The Odd Numbers are:");
        for (int i = startNumber; i <= endNumber; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
                oddSum += i;
            }
        }
        System.out.println();
        System.out.println("The total of Odd numbers is : " + oddSum);
        return oddSum;
    }

    //creating method that will show and sum Even numbers
    public static int showandsumEven(int startNumber, int endNumber) {
        int evenSum = 0;
        System.out.println("The Even Numbers are:");
        for (int i = endNumber; i >= startNumber; i--) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
                evenSum += i;
            }
        }
        System.out.println();
        System.out.println("The total of Even numbers is : " + evenSum);
        return evenSum;
    }

    //creating method that will show persent of Odd numbers
    public static void persentagingOddsum(int var1, int var2){
        int persent = 0;
        try {
            for (int i = 0; i <= endNumber; i++) {
                persent = (var2 * 100) / (var1 + var2);

            }System.out.println("Odd sum % is : " + persent);
        } catch (ArithmeticException catchmistake) {
            System.out.println("Mistake in calulation persent of Odd numbers because on START number greater than END number");
        }
    }




    //creating method that will show persent of Even numbers
    public static void persentagingEvensum(int var1, int var2) {
        int persent = 0;
        try {
            for (int i = 0; i <= endNumber; i++) {
                persent = (var1 * 100) / (var1 + var2);
            }System.out.println("Even sum % is : " + persent);
        } catch (ArithmeticException catchmistake) {
            System.out.println("Mistake in calulation persent of Even numbers because on START number greater than END number");
        }
    }
}


